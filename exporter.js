"use strict";

// var fs = require("fs");
var path = require("path");
var Http = require('http');
var moment = require("moment-timezone");
var exporter = require("datastore-exporter");

var hierarchy = path.join("tideGauge", "interpolationReady", "dailyAltitude");
var regex = new RegExp("\\" + path.sep, "g");
var tagName = hierarchy.replace(regex, ".");
var commentary = "altitude metros:\%f";

// var mareHtml = '/dhn/chm/box-previsao-mare/tabuas/%d%s%d.htm';
var requestOptions = {
            host: 'www.mar.mil.br',
            path: '',
            method: 'GET',
            headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:44.0) Gecko/20100101 Firefox/44.0',
                       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                       'Accept-Language': 'pt-BR,en-US;q=0.5',
                       'Accept-Encoding': 'gzip, deflate',
                       'Connection': 'keep-alive'
                    }
        };

var months = [
    "Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul",
    "Ago", "Set", "Out", "Nov", "Dez"
];

var years = [2011, 2012, 2013, 2014, 2015, 2016];

var harborCodes = [50210, 50225];

(function main() {
    var harborIndex = 0, yearIndex = 0, monthIndex = 0;
    // var harborIndex = 0, yearIndex = 1, monthIndex = 3;
    var endingCondition = function() {
        if(harborIndex < harborCodes.length) {
            monthIndex += 1;
            if(monthIndex >= months.length) {
                monthIndex = 0;
                yearIndex += 1;
                if(yearIndex >= years.length) {
                    harborIndex += 1;
                    yearIndex = 0;
                    if(harborIndex >= harborCodes.length) {
                        return;
                    }
                }
            }
            setTimeout(httpFetch, 500+Math.random()*200,`/dhn/chm/box-previsao-mare/tabuas/${harborCodes[harborIndex]}${months[monthIndex]}${years[yearIndex]}.htm`, endingCondition);
        }
    };
    // var req = Http.request(requestOptions, (res) => { //'http://www.google.com/index.html'
    //   console.log(`Got response: ${res.statusCode}`);
    //   // consume response body
    //   res.resume();
    // }).on('error', (e) => {
    //   console.log(`Got error: ${e.message}`);
    // });
    // req.end();
    httpFetch(`/dhn/chm/box-previsao-mare/tabuas/${harborCodes[harborIndex]}${months[monthIndex]}${years[yearIndex]}.htm`, endingCondition);
})();

function httpFetch(urlPath, callback) {
    requestOptions.path = urlPath;
    console.log(urlPath);
    var request = Http.request(requestOptions, function(response) {
        var buffer = '';
        console.log("response code: ", response.statusCode);
        // console.dir(response);
        response.setEncoding('utf8');
        response.on('data', function(chunk) {
            buffer += chunk;
        });

        response.on('end', function() {
            try {
                parser(buffer);
                if(typeof(callback) === "function") {
                    callback();
                }
            } catch(error) {
                console.log("error:");
                console.dir(error);
                throw new Error('Invalid JSON response from API!');
            }
        });
        request.on('error', function(error) {
            throw error;
        });
    });
    request.end();
}

/**
*Get data of table of water of today of target location in HTML and save in tableOfWaterJSON
* @function
* @name parser
* @return {JSON}  data of table of water of today of target location
*/
function parser(html){

    //get all elements of table
    var allTables = html.match(/<table.*?>((.|[\r\n])*?)<\/table>/igm);
    // for(i = 0; i < allTables.length; i++){
    var i = 0;
    {
        //get each 'tr' of table
        var allRows = allTables[i].match(/<tr.*?>((.|[\r\n])*?)<\/tr>/igm);
        var lastValidDate = null;
        var colRegexp = /<td.*?>(.*?)<\/td>/igm;
        // console.dir(allRows);
        for (var j=1; j<allRows.length; j++) {
            var cols = allRows[j].match(/<td.*?>((.|[\r\n])*?)<\/td>/igm);
            // var cols = colRegexp.exec(allRows[j]);
            // console.dir(cols);
            if(cols.length >= 4) {
                var date = moment.tz(cols[1].replace(colRegexp, "$1"), "DD/MM/YYYY", "America/Sao_Paulo");
                // console.log(date);
                if(date.isValid()) {
                    lastValidDate = date.clone();
                }
                else if(lastValidDate) {
                    date = lastValidDate.clone();
                }
                else {
                    continue;
                }
                var hourMinute = moment.duration(cols[2].replace(colRegexp, "$1"));
                date.add(hourMinute);
                var altitude = cols[3].replace(colRegexp, "$1");
                console.log("%s altitude:%s", date.format(), altitude);
                exporter.writeToDatastoreLog(hierarchy, date, tagName, [altitude], commentary);
            }
        }
    }
}